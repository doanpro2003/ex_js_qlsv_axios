function kiemTraRong(value, idError) {
    if (value.length == 0) {
        document.getElementById(idError).innerText =
            "Trường này không được để rỗng";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
function kiemTraMaSv(idSV, listSv, idError) {
    var index = listSv.findIndex(function (sv) {
        return (sv.ma = idSV);
    });
    if (index == -1) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).innerText = "Trường này đã tồn tại";
        return false;
    }
}
