console.log(axios());
// pending, resolve, reject
// axios ~ bat dong bo
const BASE_URL = "https://634e6c704af5fdff3a5bb501.mockapi.io";
var dssv = [];

var batLoading = function () {
    document.getElementById("loading").style.display = "flex";
};
var tatLoading = function () {
    document.getElementById("loading").style.display = "none";
};

// lay danh sach sinh vien service
var fetchDssvService = function () {
    batLoading();
    axios({
        url: `${BASE_URL}/sv`,
        method: "GET",
    })
        .then(function (response) {
            // console.log("response: ", response.data);
            renderDssv(response.data);
            dssv = response.data;
            tatLoading();
        })
        .catch(function (error) {
            // console.log("error: ", error);
            tatLoading();
        });
};
// chay lan dau khi load trang
fetchDssvService();
// render danh sach sinh vien
var renderDssv = function (listSv) {
    var contentHTML = "";
    // ${newSv.tinhDTB()}

    dssv = listSv.map(function (item) {
        return new SinhVien(
            item.ma,
            item.ten,
            item.email,
            item.matKhau,
            item.ly,
            item.toan,
            item.hoa
        );
    });
    console.log("listSv: ", listSv);
    console.log("dssv: ", dssv);

    dssv.forEach(function (sv) {
        contentHTML += `<tr>
        <td>${sv.ma}</td>
        <td>${sv.ten}</td>
        <td>${sv.email}</td>
        <td>${sv.tinhDTB()}</td>
        <td>
        <button onclick="layThongTinChiTietSv(${sv.ma
            })" class="btn btn-primary">Sửa</button>
        <button onclick=xoaSv(${sv.ma}) class="btn btn-danger">Xóa</button></td>
        </tr>`;
    });
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

var xoaSv = function (idSv) {
    batLoading();
    axios({
        url: `${BASE_URL}/sv/${idSv}`,
        method: "DELETE",
    })
        .then(function (res) {
            tatLoading();
            // goi lai api lay ds sau khi xoa thanh cong
            console.log("res: ", res);
            // thong bao thanh cong
            Swal.fire("Xóa thành công");
            fetchDssvService();
        })
        .catch(function (err) {
            tatLoading();
            Swal.fire("Xóa thất bại");
            console.log("err: ", err);
        });
};
// crud
// them sinh vien
var themSv = function () {
    var sv = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/sv`,
        method: "POST",
        data: sv,
    })
        .then(function (res) {
            Swal.fire("Thêm thành công");
            fetchDssvService();
        })
        .catch(function (err) {
            Swal.fire("Thêm thất bại");
        });
    resetForm();
};

var layThongTinChiTietSv = function (idSv) {
    // axios({
    //     url: `${BASE_URL}/sv/${idSv}`,
    //     method: "GET",
    // });
    document.getElementById("txtMaSV").disabled = true;

    axios
        .get(`${BASE_URL}/sv/${idSv}`)
        .then(function (res) {
            console.log("res: ", res.data);
            // gọi hàm show thông tin lên form
            showThongTinLenForm(res.data);
        })
        .catch(function (err) {
            console.log("err: ", err);
        });
};
var capNhatSv = function () {
    var sv = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/sv/${sv.ma}`,
        method: "PUT",
        data: sv,
    })
        .then(function (res) {
            console.log("res: ", res.data);
            fetchDssvService();
        })
        .catch(function (err) {
            console.log("err: ", err);
        });
    document.getElementById("txtMaSV").disabled = false;
    resetForm();
};
